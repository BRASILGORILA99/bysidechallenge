package com.challenge.model;

public class File extends Entry implements Comparable<File> {

    private String content;
    private int size;

    public File(String entryName, String content, Directory directory, int size, String permission) {
        super(entryName, directory, permission);
        this.content = content;
        this.size = size;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int size() {
        return size;
    }

    @Override
    public int compareTo(File otherDirectory) {
        //compare name
        if (this.name.equals(otherDirectory.name)){
            if (this.parent == null && otherDirectory.parent == null){
                return 0;
            }else if ((this.parent == null && otherDirectory.parent != null) || (this.parent != null && otherDirectory.parent == null)){
                return 1;
            }else{
                return this.parent.compareTo(otherDirectory.parent);
            }
        }else{
            return 1;
        }
    }

}