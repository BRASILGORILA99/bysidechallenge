package com.challenge.model;
import java.util.ArrayList;

public class Directory extends Entry implements Comparable<Directory> {

    ArrayList<Entry> fileList;

    public Directory(String entryName, Directory directory, String permission) {
        super(entryName, directory, permission);
        fileList = new ArrayList<Entry>();
    }

    public int size() {
        int size = 0;
        for (Entry e: fileList) {
            size += e.size();
        }
        return size;
    }

    public void addEntry(Entry entry){
        fileList.add(entry);
    }

    public boolean deleteEntry(Entry entry){
        return fileList.remove(entry);
    }

    public ArrayList<Entry> getContents() { return fileList; }
    public ArrayList<Entry> setContents(ArrayList<Entry> content) {
        this.fileList = new ArrayList<Entry>(content);
        return this.fileList;
    }

    @Override
    public int compareTo(Directory otherDirectory) {
        //compare name
        if (this.name.equals(otherDirectory.name)){
            if (this.parent == null && otherDirectory.parent == null){
                return 0;
            }else if ((this.parent == null && otherDirectory.parent != null) || (this.parent != null && otherDirectory.parent == null)){
                return 1;
            }else{
                 return this.parent.compareTo(otherDirectory.parent);
            }
        }else{
            return 1;
        }
    }
}