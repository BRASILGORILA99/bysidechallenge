package com.challenge.model;

public abstract  class Entry {

    protected Directory parent;
    protected long created;
    protected String name;
    protected String permission;

    public Entry(String n, Directory p, String permission){
        this.name = n;
        this.parent = p;
        this.created = System.currentTimeMillis();
        this.permission = permission;
    }

    public boolean delete(Entry entry){
        if(parent == null) return false;
        return parent.deleteEntry(this);
    }

    public abstract int size();

    public long getCreationTime() { return created; }
    public String getName() { return name; }
    public Directory getParent(){ return parent; }
    public String getPermission(){ return  permission; }
}