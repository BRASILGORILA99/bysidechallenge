package com.challenge;

import com.challenge.controller.DirectoryController;
import com.challenge.controller.FileController;
import com.challenge.repo.DirectoryRepository;
import com.challenge.repo.FileRepository;
import com.challenge.repo.MockDb;
import com.challenge.service.DirectoryService;
import com.challenge.service.FileService;
import com.challenge.ui.Ui;

public class Main {

    public static void main(String[] args) {
        startup();
    }
    public static void startup(){
        MockDb mockDb = new MockDb();
        bootstrap.createBaseDirectories(mockDb);
        DirectoryRepository directoryRepository = new DirectoryRepository(mockDb);
        FileRepository fileRepository = new FileRepository(mockDb);
        FileService fileService = new FileService(fileRepository);
        DirectoryService directoryService = new DirectoryService(directoryRepository);
        FileController fileController = new FileController(fileService, directoryService);
        DirectoryController directoryController = new DirectoryController(directoryService);
        Ui ui = new Ui(fileController, directoryController);
        ui.start();
    }
}
