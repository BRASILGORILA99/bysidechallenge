package com.challenge.controller;

import com.challenge.dto.DirectoryDto;
import com.challenge.model.Directory;
import com.challenge.service.DirectoryService;

public class DirectoryController {

    private DirectoryService directoryService;

    public DirectoryController(DirectoryService dirServ){
        this.directoryService = dirServ;
    }

    public Directory getRoot(){
        return this.directoryService.getRoot();
    }

    /*
     *Normally this would return a dto to the front end, but to simplify it simply returns a boolean of whether
     *it was possible to create the new directory.
     */
    public boolean createDirectory(String nome, Directory parent, String permissions){
        if(this.directoryService.createDirectory( new DirectoryDto(nome, parent, permissions)) == null){
            return false;
        }
        return true;
    }
    /*
     *This simulates the deletion of a directory from the database, normally it would receive an Id and search for it
     */
    public boolean deleteDirectory(Directory child){
        if(this.directoryService.deleteDirectory(child) == null){
            return false;
        }
        return true;
    }

    public boolean copyDirectory(Directory parent, Directory copy){
        if(this.directoryService.copyDirectory(parent, copy) == null){
            return false;
        }
        return true;
    }

    public boolean moveDirectory(Directory parent, Directory copy){
        if(this.directoryService.moveDirectory(parent, copy) == null){
            return false;
        }
        return true;
    }

}
