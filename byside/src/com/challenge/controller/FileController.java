package com.challenge.controller;

import com.challenge.dto.DirectoryDto;
import com.challenge.dto.FileDto;
import com.challenge.model.Directory;
import com.challenge.model.File;
import com.challenge.service.DirectoryService;
import com.challenge.service.FileService;

public class FileController {

    private FileService fileService;
    private DirectoryService directoryService;

    public FileController(FileService fileServ, DirectoryService dirServ){
        this.fileService = fileServ;
        this.directoryService = dirServ;
    }

    public Directory getRoot(){
        return this.directoryService.getRoot();
    }

    /*
     *Normally this would return a dto to the front end, but to simplify it simply returns a boolean of whether
     *it was possible to create the new directory.
     */
    public boolean createFile(String nome, String content, Directory parent, int size, String permissions){
        if(this.fileService.createFile( new FileDto(nome, parent, permissions, content, size)) == null){
            return false;
        }
        return true;
    }
    /*
     *This simulates the deletion of a directory from the database, normally it would receive an Id and search for it
     */
    public boolean deleteFile(File child){
        if(this.fileService.deletefile(child) == null){
            return false;
        }
        return true;
    }

    public boolean copyFile(Directory parent, File copy){
        if(this.fileService.copyFile(parent, copy) == null){
            return false;
        }
        return true;
    }

    public boolean moveFile(Directory parent, File copy){
        if(this.fileService.moveFile(parent, copy) == null){
            return false;
        }
        return true;
    }

}
