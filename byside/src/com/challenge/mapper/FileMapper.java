package com.challenge.mapper;

import com.challenge.dto.DirectoryDto;
import com.challenge.dto.FileDto;
import com.challenge.model.Directory;
import com.challenge.model.File;

/*
 * A simple mapper that just does the dto to domain conversion, mostly here to show knowledge of what mappers are and
 * what they are used for. As mentioned in the comment on the DTO itself, normally you would not receive an object such
 * as Directory on the dto.
 */
public class FileMapper {
    public static File toDomain(FileDto dto){
        return new File(dto.getName(), dto.getContent(), dto.getParent(), dto.getSize(), dto.getPermissions());
    }
}
