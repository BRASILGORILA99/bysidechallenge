package com.challenge.mapper;

import com.challenge.dto.DirectoryDto;
import com.challenge.model.Directory;

/*
* A simple mapper that just does the dto to domain conversion, mostly here to show knowledge of what mappers are and
* what they are used for. As mentioned in the comment on the DTO itself, normally you would not receive an object such
* as Directory on the dto.
*/
public class DirectoryMapper {

    public static Directory toDomain(DirectoryDto dto){
        return new Directory(dto.getName(), dto.getParent(), dto.getPermissions());
    }
}
