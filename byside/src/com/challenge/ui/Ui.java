package com.challenge.ui;

import com.challenge.controller.DirectoryController;
import com.challenge.controller.FileController;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Ui {

    private FileUi fileUi;
    private DirectoryUi directoryUi;

    public Ui(FileController fileC, DirectoryController dirC){
        this.fileUi = new FileUi(fileC);
        this.directoryUi = new DirectoryUi(dirC);
    }

    public void start(){
        System.out.println("Initializing the simulation.");
        int choice = 20;
        Scanner scan = new Scanner(System.in);

        do{
            initialOptions();
            System.out.println("Enter your option (Number from 0 to 8, then hit enter)");
            choice = scan.nextInt();
            switch (choice) {
                case 0:
                    System.out.println("...");
                    break;
                case 1:
                    if (this.fileUi.createFile()){
                        System.out.println("File created with success");
                    }else {
                        System.out.println("Error whilst creating file");
                    }
                    break;
                case 2:
                    if (this.directoryUi.createDirectory()){
                        System.out.println("Directory created with success");
                    }else {
                        System.out.println("Error whilst creating directory");
                    }
                    break;
                case 3:
                    if (this.fileUi.copyFile(1)){
                        System.out.println("File moved with success");
                    }else {
                        System.out.println("Error whilst moving file");
                    }
                    break;
                case 4:
                    if (this.directoryUi.copyDirectory(1)){
                        System.out.println("Directory moved with success");
                    }else {
                        System.out.println("Error whilst moving directory");
                    }
                    break;
                case 5:
                    if (this.fileUi.deleteFile()){
                        System.out.println("File deleted with success");
                    }else {
                        System.out.println("Error whilst deleting File");
                    }
                    break;
                case 6:
                    if (this.directoryUi.deleteDirectory()){
                        System.out.println("Directory deleted with success");
                    }else {
                        System.out.println("Error whilst deleting directory");
                    }
                    break;
                case 7:
                    if (this.fileUi.copyFile(0)){
                        System.out.println("File copied with success");
                    }else {
                        System.out.println("Error whilst copying file");
                    }
                    break;
                case 8:
                    if (this.directoryUi.copyDirectory(0)){
                        System.out.println("Directory copied with success");
                    }else {
                        System.out.println("Error whilst copying directory");
                    }
                    break;
                default:
                    System.out.println("Invalid option, please enter a number from 0 to 8, then hit enter.");
                    break;
            }

        }while (choice != 0);
        System.out.println("Closing the simulation.");
    }

    public static void initialOptions(){
        System.out.println("What would you like to do?");
        System.out.println("1: Create a file");
        System.out.println("2: Create a directory");
        System.out.println("3: Move a file");
        System.out.println("4: Move a directory");
        System.out.println("5: Delete a file");
        System.out.println("6: Delete a directory");
        System.out.println("7: Copy a file");
        System.out.println("8: Copy a directory");
        System.out.println("0: Quit");
    }
}
