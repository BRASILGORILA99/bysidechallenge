package com.challenge.ui;

import com.challenge.controller.DirectoryController;
import com.challenge.model.Directory;
import com.challenge.model.Entry;
import com.challenge.service.AdminService;

import java.util.List;
import java.util.Scanner;

public class DirectoryUi {

    private DirectoryController directoryController;

    public DirectoryUi(DirectoryController dirC){
        this.directoryController = dirC;
    }

    public boolean createDirectory(){
        Scanner scan = new Scanner(System.in);
        System.out.println("Select the directory in which you want to add a new directory.");
        Directory root  = this.directoryController.getRoot();
        List<Entry> listChildren = root.getContents();

        boolean flag = false;
        int choice = -1;
        try {
            while (!flag){
                System.out.println("Current directory: " + root.getName());
                System.out.println("0: Create directory here");
                for (int i=0;i<listChildren.size();i++){
                    if(listChildren.get(i) instanceof Directory){
                        System.out.println((i+1) +": Enter directory " + listChildren.get(i).getName());
                    }
                }
                choice = scan.nextInt();
                if (choice == 0){
                    flag = true;
                    System.out.println("Insert the name of the new directory");
                    String nome = scan.next();
                    int per = 0;
                    while (per != 1 && per != 2) {
                        System.out.println("Insert the level of permission (1: User | 2: Admin)");
                        per = scan.nextInt();
                    }
                    if (per == 2){
                        if (!AdminService.checkAdmin()){
                            return false;
                        }
                        this.directoryController.createDirectory(nome, root, "admin");
                    }else{
                        this.directoryController.createDirectory(nome, root, "user");
                    }
                }else{
                    root = (Directory) listChildren.get(choice-1);
                    if (root.getPermission().equals("admin")){
                        if(!AdminService.checkAdmin()){
                            return false;
                        }
                    }
                    listChildren = root.getContents();
                }
            }
        }catch (Exception e){
            return  false;
        }
        return true;
    }

    public boolean deleteDirectory(){
        Scanner scan = new Scanner(System.in);
        System.out.println("Select the directory you wish to delete");
        Directory root  = this.directoryController.getRoot();
        List<Entry> listChildren = root.getContents();

        boolean flag = false;
        int choice = -1;
        try {
            while (!flag){
                System.out.println("Current directory: " + root.getName());
                System.out.println("0: Delete this directory");
                for (int i=0;i<listChildren.size();i++){
                    if(listChildren.get(i) instanceof Directory){
                        System.out.println((i+1) +": Enter directory " + listChildren.get(i).getName());
                    }
                }
                choice = scan.nextInt();
                if (choice == 0){
                    flag = true;
                    if (root.compareTo(this.directoryController.getRoot()) == 0){
                        System.out.println("You cannot delete the root directory.");
                        return false;
                    }
                    this.directoryController.deleteDirectory(root);
                }else{
                    root = (Directory) listChildren.get(choice-1);
                    if (root.getPermission().equals("admin")){
                        if(!AdminService.checkAdmin()){
                            return false;
                        }
                    }
                    listChildren = root.getContents();
                }
            }
        }catch (Exception e){
            return  false;
        }
        return true;
    }

    public boolean copyDirectory(int moveOrCopy){
        Scanner scan = new Scanner(System.in);
        System.out.println("Select the directory you wish to copy");
        Directory root  = this.directoryController.getRoot();
        Directory copy = null;
        List<Entry> listChildren = root.getContents();

        boolean flag = false;
        int choice = -1;
        try {
            while (!flag){
                System.out.println("Current directory: " + root.getName());
                System.out.println("0: Copy this directory");
                for (int i=0;i<listChildren.size();i++){
                    if(listChildren.get(i) instanceof Directory){
                        System.out.println((i+1) +": Enter directory " + listChildren.get(i).getName());
                    }
                }
                choice = scan.nextInt();
                if (choice == 0){
                    flag = true;
                    if (root.compareTo(this.directoryController.getRoot()) == 0){
                        System.out.println("You cannot copy the root directory.");
                        return false;
                    }
                    copy = root;
                    if(moveOrCopy ==0){
                        copyInto(copy, 0);
                        break;
                    }else{
                        copyInto(copy, 1);
                        break;
                    }
                }else{
                    root = (Directory) listChildren.get(choice-1);
                    if (root.getPermission().equals("admin")){
                        if(!AdminService.checkAdmin()){
                            return false;
                        }
                    }
                    listChildren = root.getContents();
                }
            }
        }catch (Exception e){
            return  false;
        }
        return true;
    }

    public boolean copyInto(Directory copy, int moveOrCopy){
        Scanner scan = new Scanner(System.in);
        System.out.println("Select the directory you wish to copy into");
        Directory root  = this.directoryController.getRoot();
        List<Entry> listChildren = root.getContents();
        boolean flag = false;
        int choice = -1;
        try {
            while (!flag){
                System.out.println("Current directory: " + root.getName());
                System.out.println("0: Copy into this directory");
                for (int i=0;i<listChildren.size();i++){
                    if(listChildren.get(i) instanceof Directory){
                        System.out.println((i+1) +": Enter directory " + listChildren.get(i).getName());
                    }
                }
                choice = scan.nextInt();
                if (choice == 0){
                    if (moveOrCopy == 0){
                        if (this.directoryController.copyDirectory(root, copy)){
                            return true;
                        }else{
                            System.out.println("Directory already exists!");
                            return false;
                        }
                    }else{
                        if (this.directoryController.moveDirectory(root, copy)){
                            return true;
                        }else{
                            System.out.println("Directory already exists!");
                            return false;
                        }
                    }
                }else{
                    root = (Directory) listChildren.get(choice-1);
                    if (root.getPermission().equals("admin")){
                        if(!AdminService.checkAdmin()){
                            return false;
                        }
                    }
                    listChildren = root.getContents();
                }
            }
        }catch (Exception e){
            return  false;
        }
        return true;
    }


}
