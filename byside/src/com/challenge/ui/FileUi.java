package com.challenge.ui;

import com.challenge.controller.FileController;
import com.challenge.model.Directory;
import com.challenge.model.Entry;
import com.challenge.model.File;
import com.challenge.service.AdminService;

import java.util.List;
import java.util.Scanner;

public class FileUi{

    private FileController fileController;

    public FileUi(FileController fileC){
        this.fileController = fileC;
    }

    public boolean createFile(){
        Scanner scan = new Scanner(System.in);
        System.out.println("Select the directory in which you want to add a new File.");
        Directory root  = this.fileController.getRoot();
        List<Entry> listChildren = root.getContents();

        boolean flag = false;
        int choice = -1;
        try {
            while (!flag){
                System.out.println("Current directory: " + root.getName());
                System.out.println("0: Create File here");
                for (int i=0;i<listChildren.size();i++){
                    if(listChildren.get(i) instanceof Directory){
                        System.out.println((i+1) +": Enter directory " + listChildren.get(i).getName());
                    }
                }
                choice = scan.nextInt();
                scan.nextLine();
                if (choice == 0){
                    flag = true;
                    System.out.println("Insert the name of the new File");
                    String nome = scan.nextLine();
                    System.out.println("Insert the content of the new File");
                    String content = scan.nextLine();
                    System.out.println("Insert the size of the new file");
                    int size = scan.nextInt();
                    scan.nextLine();
                    int per = 0;
                    while (per != 1 && per != 2) {
                        System.out.println("Insert the level of permission (1: User | 2: Admin)");
                        per = scan.nextInt();
                        scan.nextLine();
                    }
                    if (per == 2){
                        if (!AdminService.checkAdmin()){
                            return false;
                        }
                        this.fileController.createFile(nome, content, root, size, "admin");
                    }else{
                        this.fileController.createFile(nome, content, root, size, "admin");
                    }
                }else{
                    root = (Directory) listChildren.get(choice-1);
                    if (root.getPermission().equals("admin")){
                        if(!AdminService.checkAdmin()){
                            return false;
                        }
                    }
                    listChildren = root.getContents();
                }
            }
        }catch (Exception e){
            return  false;
        }
        return true;
    }

    public boolean deleteFile(){
        Scanner scan = new Scanner(System.in);
        System.out.println("Select the file you wish to delete.");
        Directory root  = this.fileController.getRoot();
        List<Entry> listChildren = root.getContents();

        boolean flag = false;
        int choice = -1;
        int count =0;
        try {
            while (!flag){
                System.out.println("Current directory: " + root.getName());
                choice = -1;
                while (choice != 1 && choice != 2) {
                    System.out.println("Delete files in the current directory? (1: Yes | 2: No)");
                    choice = scan.nextInt();
                }
                if (choice == 1){
                    count=0;
                    for (int i=0;i<listChildren.size();i++){
                        if(listChildren.get(i) instanceof File){
                            count++;
                            System.out.println((i+1) +": Select File " + listChildren.get(i).getName());
                        }
                    }
                    if (count == 0){
                        System.out.println("There are no files in this directory!");
                        continue;
                    }else{
                        flag = true;
                        choice = scan.nextInt();
                        File toDelete = (File) listChildren.get(choice-1);
                        this.fileController.deleteFile(toDelete);
                    }
                }else{
                    count=0;
                    for (int i=0;i<listChildren.size();i++){
                        if(listChildren.get(i) instanceof Directory){
                            System.out.println((i+1) +": Enter directory " + listChildren.get(i).getName());
                            count++;
                        }
                    }
                    if (count == 0){
                        choice=-1;
                        System.out.println("There are no other directories to go into!");
                        while (choice != 1 && choice != 2) {
                            System.out.println("There are no files or directories! Go back to the root? (1: Yes | 2: No)");
                            choice = scan.nextInt();
                        }
                        if (choice == 1){
                            deleteFile();
                            break;
                        }else{
                            return false;
                        }
                    }
                    choice = scan.nextInt();
                    root = (Directory) listChildren.get(choice-1);
                    if (root.getPermission().equals("admin")){
                        if(!AdminService.checkAdmin()){
                            return false;
                        }
                    }
                    listChildren = root.getContents();
                    if (listChildren.size() == 0){
                        choice = -1;
                        while (choice != 1 && choice != 2) {
                            System.out.println("There are no files or directories! Go back to the root? (1: Yes | 2: No)");
                            choice = scan.nextInt();
                        }
                        if (choice == 1){
                            deleteFile();
                            break;
                        }else{
                            return false;
                        }
                    }
                }
            }
        }catch (Exception e){
            return  false;
        }
        return true;
    }

    public boolean copyFile(int copyOrMove){
        Scanner scan = new Scanner(System.in);
        System.out.println("Select the file you wish to copy");
        Directory root  = this.fileController.getRoot();
        List<Entry> listChildren = root.getContents();

        boolean flag = false;
        int choice = -1;
        int count =0;
        try {
            while (!flag){
                System.out.println("Current directory: " + root.getName());
                choice = -1;
                while (choice != 1 && choice != 2) {
                    System.out.println("Copy file in the current directory? (1: Yes | 2: No)");
                    choice = scan.nextInt();
                }
                if (choice == 1){
                    count =0;
                    for (int i=0;i<listChildren.size();i++){
                        if(listChildren.get(i) instanceof File){
                            count++;
                            System.out.println((i+1) +": Select File " + listChildren.get(i).getName());
                        }
                    }
                    if (count == 0){
                        System.out.println("There are no files in this directory!");
                        continue;
                    }else{
                        flag = true;
                        choice = scan.nextInt();
                        File toCopy = (File) listChildren.get(choice-1);
                        if (copyOrMove == 0){
                            copyInto(toCopy, 0);
                            break;
                        }else{
                            copyInto(toCopy, 1);
                            break;
                        }
                    }
                }else{
                    count=0;
                    for (int i=0;i<listChildren.size();i++){
                        if(listChildren.get(i) instanceof Directory){
                            System.out.println((i+1) +": Enter directory " + listChildren.get(i).getName());
                            count++;
                        }
                    }
                    if (count == 0){
                        choice=-1;
                        System.out.println("There are no other directories to go into!");
                        while (choice != 1 && choice != 2) {
                            System.out.println("There are no files or directories! Go back to the root? (1: Yes | 2: No)");
                            choice = scan.nextInt();
                        }
                        if (choice == 1){
                            deleteFile();
                            break;
                        }else{
                            return false;
                        }
                    }
                    choice = scan.nextInt();
                    root = (Directory) listChildren.get(choice-1);
                    if (root.getPermission().equals("admin")){
                        if(!AdminService.checkAdmin()){
                            return false;
                        }
                    }
                    listChildren = root.getContents();
                    if (listChildren.size() == 0){
                        choice = -1;
                        while (choice != 1 && choice != 2) {
                            System.out.println("There are no files or directories! Go back to the root? (1: Yes | 2: No)");
                            choice = scan.nextInt();
                        }
                        if (choice == 1){
                            deleteFile();
                            break;
                        }else{
                            return false;
                        }
                    }
                }
            }
        }catch (Exception e){
            return  false;
        }
        return true;
    }

    public boolean copyInto(File copy, int copyOrMove){
        Scanner scan = new Scanner(System.in);
        System.out.println("Select the directory you wish to copy into");
        Directory root  = this.fileController.getRoot();
        List<Entry> listChildren = root.getContents();
        boolean flag = false;
        int choice = -1;
        try {
            while (!flag){
                System.out.println("Current directory: " + root.getName());
                System.out.println("0: Copy into this directory");
                for (int i=0;i<listChildren.size();i++){
                    if(listChildren.get(i) instanceof Directory){
                        System.out.println((i+1) +": Enter directory " + listChildren.get(i).getName());
                    }
                }
                choice = scan.nextInt();
                if (choice == 0){
                    if (copyOrMove == 0){
                        if (this.fileController.copyFile(root, copy)){
                            return true;
                        }else{
                            System.out.println("Directory already exists!");
                            return false;
                        }
                    }else{
                        if (this.fileController.moveFile(root, copy)){
                            return true;
                        }else{
                            System.out.println("Directory already exists!");
                            return false;
                        }
                    }
                }else{
                    root = (Directory) listChildren.get(choice-1);
                    if (root.getPermission().equals("admin")){
                        if(!AdminService.checkAdmin()){
                            return false;
                        }
                    }
                    listChildren = root.getContents();
                }
            }
        }catch (Exception e){
            return  false;
        }
        return true;
    }
}
