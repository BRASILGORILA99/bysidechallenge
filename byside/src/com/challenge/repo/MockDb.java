package com.challenge.repo;

import com.challenge.model.Directory;
import com.challenge.model.Entry;

import java.nio.file.DirectoryStream;
import java.util.ArrayList;
import java.util.List;

public class MockDb {

    public List<Directory> directories;

    public MockDb(){
        this.directories = new ArrayList<>();
    }

    public boolean addFile(Directory directory){
        for (Directory dir: this.directories) {
            if (dir.compareTo(directory) == 0){
                return false;
            }
        }
        this.directories.add(directory);
        return true;
    }

    public boolean removeFile(Directory directory){
        return this.directories.remove(directory);
    }

    public Directory getRoot(){
        for (Directory dir: this.directories) {
            if (dir.getName() == "root" && dir.getParent() == null){
                return dir;
            }
        }
        return  null;
    }
}
