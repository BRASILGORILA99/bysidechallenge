package com.challenge.repo;

import com.challenge.model.Directory;

public class DirectoryRepository {

    private MockDb mockDb;

    public DirectoryRepository(MockDb mockDb){
        this.mockDb = mockDb;
    }

    public Directory getRoot(){
        return this.mockDb.getRoot();
    }

    /*
     *Simulates creating an entry in the database.
     */
    public Directory createDirectory(Directory child, Directory parent){
        parent.addEntry(child);
        return child;
    }
    /*
     * Simulates finding the parent directory in the database and deleting the child directory from it.
     */
    public Directory deleteDirectory(Directory child){
        Directory parent = child.getParent();
        parent.deleteEntry(child);
        return child;
    }
}
