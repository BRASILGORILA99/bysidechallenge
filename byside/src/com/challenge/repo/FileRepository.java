package com.challenge.repo;

import com.challenge.model.Directory;
import com.challenge.model.File;

public class FileRepository {
    private MockDb mockDb;

    public FileRepository(MockDb mockDb){
        this.mockDb = mockDb;
    }

    public Directory getRoot(){
        return this.mockDb.getRoot();
    }

    /*
     *Simulates creating an entry in the database.
     */
    public File createFile(File child, Directory parent){
        parent.addEntry(child);
        return child;
    }
    /*
     * Simulates finding the parent directory in the database and deleting the child file from it.
     */
    public File deleteFile(File child){
        Directory parent = child.getParent();
        parent.deleteEntry(child);
        return child;
    }
}