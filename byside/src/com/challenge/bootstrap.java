package com.challenge;

import com.challenge.model.Directory;
import com.challenge.model.File;
import com.challenge.repo.MockDb;

public class bootstrap {
    public static void createBaseDirectories(MockDb mockDb){
        //The root is the only directory that can have a null parent, as it is the base directory for the entire system
        Directory root = new Directory("root", null, "user");
        Directory admin = new Directory("admin", root, "admin");
        File adminfile = new File("adminfile1","Mock of a file with admin permissions", admin, 500,"admin");
        File adminfile2 = new File("adminfile2","Mock of a file with admin permissions", admin, 500,"admin");
        admin.addEntry(adminfile);
        admin.addEntry(adminfile2);
        Directory documents = new Directory("documents", root, "user");
        File documentfile1 = new File("documentfiles1","Mock of a file with user permissions", documents, 500,"user");
        documents.addEntry(documentfile1);
        Directory downloads = new Directory("downloads", root, "user");
        File downloadfile1 = new File("downloadfile1","Mock of a file with user permissions", documents, 500,"user");
        downloads.addEntry(downloadfile1);
        root.addEntry(admin);
        root.addEntry(documents);
        root.addEntry(downloads);
        mockDb.addFile(root);
        mockDb.addFile(admin);
        mockDb.addFile(documents);
        mockDb.addFile(downloads);
    }
}
