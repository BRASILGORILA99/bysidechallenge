package com.challenge.service;

import java.util.Scanner;

public class AdminService {
    /*
    * Very simple method to check admin permissions, normally something that should be checked in a different way.
    * The password is "qwerty"
    */
    public static boolean checkAdmin(){
        Scanner scan = new Scanner(System.in);
        System.out.println("Type the admin password");
        String nome = scan.next();
        if (!nome.equals("qwerty")){
            System.out.println("You have inserted the wrong password");
            return false;
        }
        System.out.println("Correct password inserted");
        return true;
    }
}
