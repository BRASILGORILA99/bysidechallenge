package com.challenge.service;

import com.challenge.dto.DirectoryDto;
import com.challenge.dto.FileDto;
import com.challenge.mapper.DirectoryMapper;
import com.challenge.mapper.FileMapper;
import com.challenge.model.Directory;
import com.challenge.model.File;
import com.challenge.repo.DirectoryRepository;
import com.challenge.repo.FileRepository;

public class FileService {

    private FileRepository fileRepository;

    public FileService(FileRepository fileRepo){
        this.fileRepository = fileRepo;
    }

    /*
     *Simulates receiving a dto and using the mapper to convert to domain and send a request to the repo to persist it
     * in the database
     */
    public File createFile(FileDto dto){
        File child = FileMapper.toDomain(dto);
        return this.fileRepository.createFile(child, dto.getParent());
    }
    /*
     *Simulates receiving a child file, finding the parent and deleting the child from the parent
     */
    public File deletefile(File child){
        File deleted = this.fileRepository.deleteFile(child);
        return deleted;
    }

    public File copyFile(Directory parent, File copy){
        for (int i=0;i<parent.getContents().size();i++){
            if (parent.getContents().get(i) instanceof File){
                if (((File) parent.getContents().get(i)).compareTo(copy) == 0){
                    return null;
                }
            }
        }
        File copyDirectory = new File(copy.getName(), copy.getContent(), parent, copy.size(), copy.getPermission());
        return this.fileRepository.createFile(copyDirectory, parent);
    }

    public File moveFile(Directory parent, File copy){
        for (int i=0;i<parent.getContents().size();i++){
            if (parent.getContents().get(i) instanceof File){
                if (((File) parent.getContents().get(i)).compareTo(copy) == 0){
                    return null;
                }
            }
        }
        File copyDirectory = new File(copy.getName(), copy.getContent(), parent, copy.size(), copy.getPermission());
        this.fileRepository.deleteFile(copy);
        return this.fileRepository.createFile(copyDirectory, parent);
    }
}
