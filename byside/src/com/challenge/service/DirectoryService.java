package com.challenge.service;

import com.challenge.dto.DirectoryDto;
import com.challenge.mapper.DirectoryMapper;
import com.challenge.model.Directory;
import com.challenge.repo.DirectoryRepository;

import java.nio.file.DirectoryStream;

public class DirectoryService {

    private DirectoryRepository directoryRepository;

    public DirectoryService(DirectoryRepository dirRepo){
        this.directoryRepository = dirRepo;
    }
    public Directory getRoot(){
        return this.directoryRepository.getRoot();
    }

    /*
     *Simulates receiving a dto and using the mapper to convert to domain and send a request to the repo to persist it
     * in the database
     */
    public Directory createDirectory(DirectoryDto dto){
        Directory child = DirectoryMapper.toDomain(dto);
        return this.directoryRepository.createDirectory(child, dto.getParent());
    }
    /*
     *Simulates receiving a child directory, finding the parent and deleting the child from the parent
     */
    public Directory deleteDirectory(Directory child){
        Directory deleted = this.directoryRepository.deleteDirectory(child);
        return deleted;
    }

    public Directory copyDirectory(Directory parent, Directory copy){
        for (int i=0;i<parent.getContents().size();i++){
            if (parent.getContents().get(i) instanceof Directory){
                if (((Directory) parent.getContents().get(i)).compareTo(copy) == 0){
                    return null;
                }
            }
        }
        Directory copyDirectory = new Directory(copy.getName(), parent, copy.getPermission());
        copyDirectory.setContents(copy.getContents());

        return this.directoryRepository.createDirectory(copyDirectory, parent);
    }

    public Directory moveDirectory(Directory parent, Directory copy){
        for (int i=0;i<parent.getContents().size();i++){
            if (parent.getContents().get(i) instanceof Directory){
                if (((Directory) parent.getContents().get(i)).compareTo(copy) == 0){
                    return null;
                }
            }
        }
        Directory copyDirectory = new Directory(copy.getName(), parent, copy.getPermission());
        copyDirectory.setContents(copy.getContents());
        this.deleteDirectory(copy);
        return this.directoryRepository.createDirectory(copyDirectory, parent);
    }
}
