package com.challenge.dto;

import com.challenge.model.Directory;

/*
 * This is just a simulation of a dto you would receive, normally it would utilize a String of the Directory's Id
 * and search for it in the database, but for simplicity's sake I've simply utilized the actual Directory class,
 * as this is mainly used to show knowledge of what DTO's are.
 */
public class FileDto {
    String name;
    String content;
    int size;
    Directory parent;
    String permissions;

    public FileDto(String name, Directory dir, String permissions, String content, int size){
        this.name = name;
        this.parent = dir;
        this.permissions = permissions;
        this.content = content;
        this.size = size;
    }
    public String getName(){ return  this.name; }
    public Directory getParent(){ return  this.parent; }
    public String getPermissions(){ return  this.permissions; }
    public String getContent(){ return  this.content; }
    public int getSize(){ return  this.size; }
}
